package resolucion;

public class Circulo {
//	private double radio;
//	private Punto centro;
	double radio; // la clase Dibujador espera que estos atributos no estén en private.
	Punto centro; // la clase Dibujador espera que estos atributos no estén en private.

	public Circulo(double centro_x, double centro_y, double radio) {
		setRadio(radio);
		this.centro = new Punto(centro_x, centro_y);
	}
	public void imprimir() {
		System.out.println("x centro: " + this.centro.getX() + "\ny centro: "+ this.centro.getY() + "\nradio: " + getRadio());
	}
	public double perimetro() {
		return Math.PI * 2 * this.radio;
	}
	public double superficie() {
		return Math.PI * Math.pow(this.radio, 2);
	}
	public void escalar(double factor) {
		this.radio *= factor;
	}
	public void desplazar(double desp_x, double desp_y) {
		this.centro.desplazar(desp_x, desp_y);
	}
	public double getRadio(){
		return this.radio;
	}
	public void setRadio(double radio) {
		this.radio = radio;
	}
	
	
}
