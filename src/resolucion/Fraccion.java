package resolucion;

public class Fraccion {
    private int numerador;
    private int denominador;
    
    public Fraccion(int numerador, int denominador) {
    	setNumerador(numerador);
    	setDenominador(denominador);
    	if (this.numerador < 0 && this.denominador < 0) {
    		this.numerador *= -1;
    		this.denominador *= -1;
    	}
    }
//    public Fraccion() {
//    	
//    }
    public int getNumerador() {
    	return numerador;
    }
    
    public void setNumerador(int numerador) {
    	this.numerador = numerador;
    }

    public int getDenominador() {
    	return denominador;
    }
    
    public void setDenominador(int denominador) {
    	if (denominador == 0) {
    		System.out.println("Denominador inválido");
    		return;
    	}
    	this.denominador = denominador;
    }
    public void imprimir() {
    	if (this.denominador != 0)
			System.out.println(this.numerador+"/"+this.denominador);
    	else
    		System.out.println("Fracción inválida");
    }
    public void invertirSigno() {
    	if (this.numerador < 0 || (this.numerador > 0 && this.denominador > 0))
    		this.numerador *= -1;
    	else
    		this.denominador *= -1;
    }
    public void invertir() {
    	if (this.numerador != 0) {
			int a = this.numerador;
			this.numerador = this.denominador;
			this.denominador = a;
    	}
    	else
    		System.out.println("No se puede invertir la fracción");
    }
    public double aDouble() {
		return this.numerador * 1.0 /this.denominador;
    }
    public void reducir() {
    	int mcd = Main.mcd(this.numerador, this.denominador);
    	this.numerador = this.numerador/mcd;
    	this.denominador = this.denominador/mcd;
    }
	static Fraccion producto(Fraccion q1, Fraccion q2) {
		int n = q1.getNumerador()*q2.getNumerador(), d = q1.getDenominador()*q2.getDenominador();
		Fraccion f = new Fraccion(n,d);
		f.reducir();
		return f;
	}
    static Fraccion suma(Fraccion q1, Fraccion q2) {
    	int n = 0, d = 0;
    	if (q1.getDenominador() == q2.getDenominador()) {
    		d = q1.getDenominador();
    		n = q1.getNumerador() + q2.getNumerador();
    	}
    	else {
    		d = q1.getDenominador() * q2.getDenominador();
    		n = (d / q1.getDenominador()* q1.getNumerador()) + (d/q2.getDenominador()*q2.getNumerador());
    	}
    	Fraccion f = new Fraccion(n,d);
    	f.reducir();
    	return f;
    }
}
