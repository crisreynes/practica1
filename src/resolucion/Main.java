package resolucion;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
//		Dibujador dib = new Dibujador();
		Punto p = new Punto(2,4);
		p.imprimir();
		Circulo c = new Circulo(1000,500,1);
		c.imprimir();
		System.out.println(c.perimetro());
		System.out.println(c.superficie());
//		dib.dibujar(c);
	}
	public static int mcd(int a, int b) {
		if (a % b == 0)
			return b;
		return mcd(b,a%b);
	}
}
