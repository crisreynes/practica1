package resolucion;

public class Punto {
//	private double x;
//	private double y;
	double x; // El código de la clase dibujador espera que x sea visible
	double y; // El código de la clase dibujador espera que y sea visible

	
	public Punto() {
		setX(0);
		setY(0);
	}
	public Punto(double x, double y) {
		setX(x);
		setY(y);
	}
	
	public void imprimir() {
		System.out.println("("+this.x+","+this.y+")");
	}
	public void desplazar(double desp_x, double desp_y) {
		this.x += desp_x;
		this.y += desp_y;
	}
	public static double distancia(Punto p1, Punto p2) {
		return Math.sqrt(Math.pow(p1.getX()-p2.getX(), 2)+Math.pow(p1.getY()-p2.getY(), 2));
	}
	
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
}
